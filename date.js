const btn = document.getElementById('begin');

let liveDT = setInterval(() =>{
    let today = new Date();

    let date = today.toDateString().split(" ");
    let time = today.toLocaleTimeString();

    document.getElementById('livedatetime').innerHTML = date[0]+" "+date[2]+" "+date[1]+" "+date[3]+" "+time
},1000)


btn.addEventListener("click", updateBtn);

function updateBtn(){
    if(btn.textContent === "Start"){
        btn.textContent = "Stop";
        btn.style.backgroundColor = "red"
        start();
    }else if(btn.textContent === "Stop"){
        btn.textContent = "Clear";
        btn.style.backgroundColor = "gold"
        stop();
        calTime();
        pay();
    }else if(btn.textContent == "Clear"){
        btn.textContent = "Start"
        btn.style.backgroundColor = "green"
        clear();
    }
}


var splitStartTime;
var splitSecStart;
function start(){
    let today = new Date();
    let timeStart = today.toLocaleTimeString();
    document.getElementById("startTime").innerHTML =  timeStart
    splitStartTime = timeStart.split(":")
    splitSecStart = splitStartTime[2].split(" ")
}


var splitStopTime;
var splitSecStop
function stop(){
    let today = new Date();
    let timeStop = today.toLocaleTimeString();
    document.getElementById("stopTime").innerHTML = timeStop
    splitStopTime = timeStop.split(":")
    splitSecStop = splitStopTime[2].split(" ")
}


function clear(){
    document.getElementById("startTime").innerHTML = "00:00:00"
    document.getElementById("stopTime").innerHTML = "00:00:00"
    document.getElementById("minute").innerHTML = "0"
    document.getElementById("pay").innerHTML = "0"
}

var toatalMn;
//calculate minute between start time and stop time
function calTime(){
    //get canada date format
    let today = new Date();
    let ca = new Intl.DateTimeFormat('en-CA').format(today)

    //initailize date time in constructor
    let startTime = new Date(ca+" "+splitStartTime[0]+":"+splitStartTime[1]+":"+splitSecStart[0]);
    let getStartTime = startTime.toLocaleTimeString().split(":")

    let stopTime = new Date(ca+" "+splitStopTime[0]+":"+splitStopTime[1]+":"+splitSecStop[0]);
    let getStopTime = stopTime.toLocaleTimeString().split(":")

    //calculate 
    let totalTime = stopTime - startTime
    toatalMn = Math.floor(totalTime/60000)

    document.getElementById("minute").innerHTML = toatalMn
}

//Find payment
function pay(){
    let payment = 0;
    let calPay = setInterval(() =>{
        if(toatalMn<=60){
            if(toatalMn >= 0 && toatalMn <=15){
                payment += 500;
                document.getElementById("pay").innerHTML = payment
            }else if(toatalMn >= 16 && toatalMn <=30){
                payment += 1000;
                document.getElementById("pay").innerHTML = payment
            }else if(toatalMn >= 31 && toatalMn <=60){
                payment += 1500;
                document.getElementById("pay").innerHTML = payment
            }
            clearInterval(calPay)
        }else{
            toatalMn = toatalMn - 60
            payment += 1500;
        }
    },0)
}
